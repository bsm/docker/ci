DOCKER=$(shell which docker || which podman)

BUILDS=$(patsubst %/Dockerfile,build/%,$(sort $(wildcard */Dockerfile)))
PUSHES=$(patsubst %/Dockerfile,push/%,$(sort $(wildcard */Dockerfile)))

build: pull $(BUILDS)
push: $(PUSHES)

.PHONY: build push

# ---------------------------------------------------------------------

DEPS=$(shell grep -h 'FROM' ./*/Dockerfile | grep -v blacksquaremedia | sed -e 's/FROM //')

pull:
	for dep in $(DEPS); do docker pull $$dep; done

build/%: %
	dep=$$(grep -h 'FROM blacksquaremedia/ci:' $(notdir $@)/Dockerfile | sed -e 's/FROM blacksquaremedia\/ci\:/build\//'); test -n "$$dep" && $(MAKE) $$dep || true
	$(DOCKER) build --force-rm --compress -t blacksquaremedia/ci:$< $</
	$(DOCKER) tag blacksquaremedia/ci:$< blacksquaremedia/drone:$<

push/%: % build/%
	$(DOCKER) push blacksquaremedia/ci:$<
	$(DOCKER) push blacksquaremedia/drone:$<
